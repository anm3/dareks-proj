﻿using System;

namespace GoodByeAzurePack.Core.SocketsSignal
{
	public struct ConnectedId
	{
		private string value { get; set; }

		public ConnectedId(string value)
		{
			this.value = value;
		}

		public override bool Equals(object obj)
		{
			if (obj is ConnectedId anyProductPartNumber)
			{
				return this.Equals(anyProductPartNumber);
			}

			return false;
		}

		public bool Equals(ConnectedId other)
		{
			return this.value.Equals(other.value, StringComparison.InvariantCulture);
		}

		public override int GetHashCode()
		{
			return this.value?.GetHashCode(StringComparison.Ordinal) ?? 0;
		}

		public override string ToString()
		{
			return this.value;
		}

		public static implicit operator ConnectedId(string value)
		{
			return new ConnectedId(value);
		}

		public static explicit operator string(ConnectedId connectionId)
		{
			return connectionId.value;
		}

		public static bool operator ==(ConnectedId left, ConnectedId right)
		{
			return left.value == right.value;
		}

		public static bool operator !=(ConnectedId left, ConnectedId right)
		{
			return !(left == right);
		}
	}
}