﻿using GoodByeAzurePack.Core.Extensions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace GoodByeAzurePack.Core.SocketsSignal
{
	public class DashboardHub : Hub, IJob
    {
		private readonly IHubContext<DashboardHub> _hubContext;
		private readonly ILogger<DashboardHub> _logger;

		private static ConcurrentDictionary<ConnectedId, GraphData> ConnectedIdsAndDashBoardVersions = new();
		private static KeyValuePair<GraphDataHash, GraphData> CurrentVersionAndValue = new();
		private static bool AnyConnections = false;

		public static Stopwatch PollingTimer;

		public DashboardHub(IHubContext<DashboardHub> hubContext, ILogger<DashboardHub> logger)
		{
			_hubContext = hubContext;
			_logger = logger;
		}
		public Task Execute(IJobExecutionContext context)
		{
			if (ConnectedIdsAndDashBoardVersions.Any())
			{
				if (!AnyConnections)
				{
					_logger.LogInformation("Polling started");
					PollingTimer = new Stopwatch();
				}
				AnyConnections = true;
			}
			else 
			{
				if (AnyConnections)
				{
					_logger.LogInformation($"Polling stopped after running for {PollingTimer.Elapsed.TotalHours} hour(s)");
					PollingTimer.Stop();
				}
				AnyConnections = false;
			}
			if (AnyConnections)
			{
				SetCurrentVersionAndValue(force: true);
				return this._hubContext.Clients.AllExcept(GetConnectionsWithCurrentVersionAndSetAllConnectionsToCurrentVersion()).SendAsync("OnVersionChange", CurrentVersionAndValue.Value.Response);
			}
			return Task.CompletedTask;
		}

		private IReadOnlyList<string> GetConnectionsWithCurrentVersionAndSetAllConnectionsToCurrentVersion()
		{
			var connectionsWithCurrentVersion = ConnectedIdsAndDashBoardVersions.Where(l => Utils.GetHashString(l.Value.Response) == CurrentVersionAndValue.Key).Select(l => l.Key.ToString()).ToList();
			foreach (var key in ConnectedIdsAndDashBoardVersions.Keys.ToList())
			{
				ConnectedIdsAndDashBoardVersions[key] = CurrentVersionAndValue.Value;
			}
			return connectionsWithCurrentVersion;
		}

		private void SetCurrentVersionAndValue(bool force = false)
		{
			if (force || CurrentVersionAndValue.IsEmpty())
			{
				var currentValue = Utils.Base64Encode(APIStuff.ApiWrapper.GetFunnelDataAsString());
				var currentVersion = Utils.GetHashString(currentValue);
				CurrentVersionAndValue = new KeyValuePair<GraphDataHash, GraphData>(currentVersion, new GraphData() { Response = currentValue });
			}
		}

		public override Task OnConnectedAsync()
		{
			SetCurrentVersionAndValue();
			ConnectedIdsAndDashBoardVersions.TryAdd(Context.ConnectionId, CurrentVersionAndValue.Value);
			this._hubContext.Clients.Client(Context.ConnectionId).SendAsync("OnVersionChange", CurrentVersionAndValue.Value.Response);
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			ConnectedIdsAndDashBoardVersions.TryRemove(Context.ConnectionId, out GraphData _);
			return base.OnDisconnectedAsync(exception);
		}
	}
}
