﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;

namespace GoodByeAzurePack.Core.APIStuff
{
	public static class ApiWrapper 
	{
		private static readonly HttpClient client = new();

		public static string GetFunnelDataAsString(DateTime? fromDate = null, DateTime? toDate = null) => JsonConvert.SerializeObject(GetFunnelData(fromDate, toDate).Select(i => new FunnelItemDTO() { Label = i.Key, Total = i.Value }).ToList());
		private static Dictionary<string, int> GetFunnelData(DateTime? fromDate, DateTime? toDate)
		{
			var request = new HttpRequestMessage
			{
				Method = HttpMethod.Get,
				RequestUri = new Uri("https://azfunc-web-ucfunctionsvnet-we-live-svc.azurewebsites.net/api/GetMigrationFunnelData?code=Dpv5JpI5pokQ4asKlA3zrqbEFaPhn9B9uJCBPzksNEKlraUj59yjBw=="),
			};
			if (fromDate != null && toDate != null && fromDate != DateTime.MinValue && toDate != DateTime.MinValue && toDate > fromDate)
			{
				request.Content = new StringContent(System.Text.Json.JsonSerializer.Serialize(new FunnelDataDateRange()
				{
					FromDate = fromDate.GetValueOrDefault().Date,
					ToDate = toDate.GetValueOrDefault().Date
				}), Encoding.UTF8, MediaTypeNames.Application.Json);
			}
			var response = client.Send(request);
			response.EnsureSuccessStatusCode();
			var responseBody = response.Content.ReadAsStream();
			using var reader = new StreamReader(responseBody, Encoding.UTF8);
			return System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, int>>(reader.ReadToEnd());

		}
	}
	public class FunnelDataDateRange
	{
		public FunnelDataDateRange()
		{
		}

		public DateTime FromDate { get; set; }
		public DateTime ToDate { get; set; }
	}
}
