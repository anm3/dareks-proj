﻿using System.Security.Cryptography;
using System.Text;

namespace GoodByeAzurePack.Core
{
	public static class Utils
	{
		public static string Base64Encode(string plainText)
		{
			if (string.IsNullOrEmpty(plainText))
			{
				return "";
			}
			var plainTextBytes = Encoding.GetEncoding(28591).GetBytes(plainText);
			return System.Convert.ToBase64String(plainTextBytes);
		}

		public static string Base64Decode(string base64EncodedData)
		{
			if (string.IsNullOrEmpty(base64EncodedData))
			{
				return "";
			}
			var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
			return Encoding.GetEncoding(28591).GetString(base64EncodedBytes);
		}

		private static byte[] GetHash(string inputString)
		{
			using HashAlgorithm algorithm = SHA256.Create();
			return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
		}

		public static string GetHashString(string inputString)
		{
			StringBuilder sb = new();
			foreach (byte b in GetHash(inputString))
				sb.Append(b.ToString("X2"));

			return sb.ToString();
		}
	}
}
