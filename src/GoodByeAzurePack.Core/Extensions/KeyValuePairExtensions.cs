﻿using GoodByeAzurePack.Core.SocketsSignal;
using System.Collections.Generic;

namespace GoodByeAzurePack.Core.Extensions
{
	public static class KeyValuePairExtensions
	{
		public static bool IsEmpty(this KeyValuePair<GraphDataHash, GraphData> input) => string.IsNullOrEmpty(input.Key.ToString());
	}
}
