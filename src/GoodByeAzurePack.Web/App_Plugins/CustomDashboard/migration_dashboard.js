angular.module("umbraco").controller("MigrationReportController", function ($scope, $http, localizationService, angularHelper) {
	if (!$scope.model) {
		$scope.model = {};
	}
	$scope.model.fromDate = new Date();
	$scope.model.toDate = new Date();
	var fromDate = $scope.model.fromDate.toIsoDateString();
	var toDate = $scope.model.toDate.toIsoDateString();


	$scope.model.dateConfig = {
		enableTime: false,
		dateFormat: "Y-m-d"
	};


	$scope.model.buttonState = "init";
	var localizeList = [
		"MigrationReport_MigrationReportFromDate",
		"MigrationReport_MigrationReportToDate"
	];

	$scope.translations = {};

	localizationService.localizeMany(localizeList).then(function (data) {
		for (var i = 0; i < localizeList.length; ++i) {
			$scope.translations[localizeList[i]] = data[i];
		}
	});


	var connection = new signalR.HubConnectionBuilder().withUrl("/dashboardHub").build();
	var createDataStructure = function (theCurrentValue) {
		var myData = JSON.parse(atob(theCurrentValue));
		return {
			labels: myData.map(function (item) { return item.Label }),
			colors: ['#FFB178', '#FF3C8E'],
			values: myData.map(function (item) { return item.Total })
		};
	}

	var graph;
	connection.on("OnVersionChange", function (message) {
		if (!graph) {
			graph = new FunnelGraph({
				container: '.funnel',
				gradientDirection: 'horizontal',
				data: createDataStructure(message),
				direction: 'horizontal',
				width: 800,
				height: 300,
				displayPercent: false
			});
			graph.draw();
		} else {
			var data = createDataStructure(message);
			//calling it twice because of a bug in the library
			graph.updateData(data); graph.updateData(data);
		}
	});

	async function start() {
		try {
			await connection.start();
		} catch (err) {
			console.log(err);
			setTimeout(start, 5000);
		}
	};

	connection.onclose(async () => {
		await start();
	});

	// Start the connection.
	start();

	

});
